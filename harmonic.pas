unit harmonic;
  {a unit to find a sum of harmonic numbers}
  interface
    function harmonicFind(reach: Int64): real;
  implementation
    function harmonicFind(reach: Int64): real;
    var
      from: Int64;
      sum: real;
    begin
      sum := 0;
      for from := 1 to reach do
        sum := sum + (1/from);
      harmonicFind := sum;
    end;
end.
