program harmonicTest;
  uses sysutils, harmonic;
  var
    reach: Int64;
    sum: real;
  begin
    reach := StrToInt64(ParamStr(1));
    sum := harmonicFind(reach);
    writeln('sum: ', FormatFloat('0.00', sum));
  end.
